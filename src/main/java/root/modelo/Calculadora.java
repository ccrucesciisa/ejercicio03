/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package root.modelo;

/**
 *
 * @author ccruces
 */
public class Calculadora {

    /**
     * @return the suma
     */
    public int getSuma() {
        return suma;
    }

    /**
     * @param suma the suma to set
     */
    public void setSuma(int suma) {
        this.suma = suma;
    }

    /**
     * @return the n1
     */
    public int getN1() {
        return n1;
    }

    private int n1;
    private int n2;
    private int suma;

    /**
     * @param n1 the n1 to set
     */
    public void setN1(int n1) {
        this.n1 = n1;
    }

    /**
     * @return the n2
     */
    public int getN2() {
        return n2;
    }

    /**
     * @param n2 the n2 to set
     */
    public void setN2(int n2) {
        this.n2 = n2;
    }

    public int sumar() {

        //si los valores son mayores a 1000 entonces rechazar
        
        return this.getN1() + this.getN2();

    }

    public int sumar(int n1, int n2) {
        this.setN1(n1);
        this.setN2(n2);

        return sumar();

    }

}
